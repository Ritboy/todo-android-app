package com.example.todolist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import com.shrikanthravi.collapsiblecalendarview.data.Day
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import kotlinx.android.synthetic.main.activity_main.*
import java.time.DateTimeException
import java.time.DayOfWeek
import java.util.*

const val EXTRA_MESSAGE = "com.example.todolist.MESSAGE"

class MainActivity : AppCompatActivity() {

    lateinit var calendarView: CollapsibleCalendar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        calendarView = findViewById<CollapsibleCalendar>(R.id.calendarView)
        calendarView.setCalendarListener(CalendarListener())
        supportActionBar?.title =  getString(R.string.action_bar_today)
    }

    inner class CalendarListener : CollapsibleCalendar.CalendarListener {
        override fun onClickListener() {
        }

        override fun onDataUpdate() {
            if (calendarView.selectedItem == null) {
                supportActionBar?.title =  getString(R.string.action_bar_today)
            }
        }

        override fun onDayChanged() {
        }

        override fun onDaySelect() {
            supportActionBar?.title = if (calendarView.isToday(calendarView.selectedDay))
                getString(R.string.action_bar_today)
                else "${calendarView.selectedDay?.day}/${((calendarView.selectedDay as Day).month) + 1}"
        }

        override fun onItemClick(v: View) {
        }

        override fun onMonthChange() {
        }

        override fun onWeekChange(position: Int) {
        }
    }
}
